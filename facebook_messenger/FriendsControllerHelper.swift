//
//  FriendsControllerHelper.swift
//  facebookMessenger
//
//  Created by Juan Ramirez on 5/21/17.
//  Copyright © 2017 Juan Ramirez. All rights reserved.
//

import UIKit
import CoreData

extension FriendsViewController {
    
    func clearData() {
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        if let context = delegate?.persistentContainer.viewContext {
            
            do {
                let entityNames = ["Friend", "Message"]
                
                for entityName in entityNames {
                    let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
                    let objects = try context.fetch(fetchRequest) as? [NSManagedObject]
                    
                    for object in objects! {
                        context.delete(object)
                    }
                }
                
                try context.save()
                
            } catch let err {
                print(err)
                }
            }
    }
    
    func setupData() {
        
        clearData()
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        if let context = delegate?.persistentContainer.viewContext {
            
            let mark = NSEntityDescription.insertNewObject(forEntityName: "Friend", into: context) as! Friend
            
            mark.name = "Mark Zuckerberg"
            mark.profileImageName = "zuckprofile"
            
            let markMessage = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as! Message
            markMessage.friend = mark
            markMessage.text = "Hello my name is Mark. Nice to meet you..."
            markMessage.date = NSDate()
            
            
            let steve = NSEntityDescription.insertNewObject(forEntityName: "Friend", into: context) as! Friend
            steve.name = "Steve Jobs"
            steve.profileImageName = "steve_profile"
            
            let steveMessage = NSEntityDescription.insertNewObject(forEntityName: "Message", into: context) as! Message
            steveMessage.friend = steve
            steveMessage.text = "Apple's new campus is amazing you should come take a tour"
            steveMessage.date = NSDate()
            
            do {
                try context.save()
            } catch let err {
                print(err)
            }
            
            //messages = [markMessage, steveMessage]
        }
        
        loadData()
    }
    
    
    // Load Data from CoreData
    func loadData() {
        
        let delegate = UIApplication.shared.delegate as? AppDelegate
        
        if let context = delegate?.persistentContainer.viewContext {
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Message")
            
            do {
                messages = try context.fetch(fetchRequest) as? [Message]
            } catch let err {
                print(err)
            }
            
        }
    }
    
}
